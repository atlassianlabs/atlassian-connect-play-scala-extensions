package com.atlassian.connect.playscala.store.redis

import com.atlassian.connect.playscala.model.{ ClientKey, AcHostModel, AcHostModelStore }
import redis.clients.jedis.Transaction
import scala.collection.JavaConverters._
import com.atlassian.connect.playscala.AcConfig
import scalaz.syntax.std.option._
import scalaz.syntax.std.boolean._
import org.sedis.Pool
import com.typesafe.plugin.RedisPlugin
import org.apache.commons.codec.net.URLCodec

object RedisPool {
  def sedisPool: Pool = play.api.Play.current.plugin(classOf[RedisPlugin]).
    getOrElse(throw new IllegalStateException("Redis plugin not available")).sedisPool
}

class AcHostModelRedisStore(acConfig: AcConfig) extends AcHostModelStore {

  import RedisPool.sedisPool

  private val AcHostRedisKey = "ac_host"
  private val AcHostByUrlRedisKey = "ac_host_url"
  private val AllAcHostRedisKey = "ac_host_all"

  private val FieldPublicKey = "publicKey"
  private val FieldBaseUrl = "baseUrl"
  private val FieldSharedSecret = "sharedSecret"
  private val FieldProductType = "productType"
  private val FieldDescription = "description"

  private val codec: URLCodec = new URLCodec("UTF-8")
  private val KEY_SEPARATOR = "#"
  private def buildKey(components: String*): String = components.map(codec.encode).mkString(KEY_SEPARATOR)

  private def buildAcHostKey(clientKey: ClientKey): String = buildKey(AcHostRedisKey, clientKey.value)
  private def buildAcHostKey(acHost: AcHostModel): String = buildAcHostKey(acHost.key)

  private def buildAcHostMap(acHost: AcHostModel): Map[String, String] = Map(
    FieldPublicKey -> acHost.publicKey,
    FieldBaseUrl -> acHost.baseUrl,
    FieldSharedSecret -> acHost.sharedSecret,
    FieldProductType -> acHost.productType,
    FieldDescription -> (acHost.description | "")
  )

  private def buildAcHostByUrlKey(baseUrl: String): String = buildKey(AcHostByUrlRedisKey, baseUrl)
  private def buildAcHostByUrlKey(acHost: AcHostModel): String = buildAcHostByUrlKey(acHost.baseUrl)

  private val buildAllAcHostKey = buildKey(AllAcHostRedisKey)

  def save(acHost: AcHostModel): AcHostModel = {
    sedisPool.withJedisClient { jedis =>

      val tx: Transaction = findByKey(acHost.key).fold(jedis.multi()) { oldAcHost =>
        val acHostByUrlKey: String = buildAcHostByUrlKey(oldAcHost)
        // jedis.watch(acHostByUrlKey) // we could do check-and-set using watch
        val tx: Transaction = jedis.multi()
        tx.srem(acHostByUrlKey, oldAcHost.key.value)
        if (acConfig.isDev) {
          tx.srem(buildAllAcHostKey, oldAcHost.key.value)
        }
        tx
      }
      tx.hmset(buildAcHostKey(acHost), buildAcHostMap(acHost).asJava)
      tx.sadd(buildAcHostByUrlKey(acHost), acHost.key.value)
      if (acConfig.isDev) {
        tx.sadd(buildAllAcHostKey, acHost.key.value)
      }
      tx.exec()
      acHost
    }
  }

  def findByUrl(baseUrl: String): Option[AcHostModel] =
    sedisPool.withClient { redis =>
      val set: Set[String] = redis.smembers(buildAcHostByUrlKey(baseUrl))
      set.headOption flatMap (ClientKey andThen findByKey)
    }

  def findByKey(clientKey: ClientKey): Option[AcHostModel] = {
    sedisPool.withClient { redis =>
      val fields: Map[String, String] = redis.hgetAll(buildAcHostKey(clientKey))
      if (fields.isEmpty) None else {
        Some(AcHostModel(
          id = None,
          key = clientKey,
          publicKey = fields(FieldPublicKey),
          baseUrl = fields(FieldBaseUrl),
          productType = fields.get(FieldProductType) | "",
          description = fields.get(FieldDescription),
          sharedSecret = fields(FieldSharedSecret)
        ))
      }
    }
  }

  def all(): Seq[AcHostModel] = !acConfig.isDev ? Seq[AcHostModel]() |
    sedisPool.withClient { redis =>
      redis.smembers(buildAllAcHostKey).toSeq flatMap (k => findByKey(ClientKey(k)).toSeq)
    }

}
