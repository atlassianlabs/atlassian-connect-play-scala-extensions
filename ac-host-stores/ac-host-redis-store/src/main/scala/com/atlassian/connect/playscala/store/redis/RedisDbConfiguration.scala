package com.atlassian.connect.playscala.store.redis

import com.atlassian.connect.playscala.store.DbConfiguration
import com.atlassian.connect.playscala.model.AcHostModelStore
import com.atlassian.connect.playscala.{ PlayAcConfigured, AcConfigured }

trait RedisDbConfiguration extends DbConfiguration with AcConfigured {
  override def acHostModelStore: AcHostModelStore = new AcHostModelRedisStore(acConfig)
}

object RedisDbConfiguration extends RedisDbConfiguration with PlayAcConfigured
