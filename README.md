# Extensions for Atlassian Connect Play Scala Module

## Description

This is a collection of extensions for the Atlassian Connect Play Scala module to help develop Atlassian Connect add-ons quickly without reinveting the wheels.