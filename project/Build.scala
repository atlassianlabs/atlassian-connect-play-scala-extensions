import sbt._
import com.typesafe.sbt.SbtScalariform.scalariformSettings
import net.virtualvoid.sbt.graph.Plugin.graphSettings
import Keys._
import sbtrelease.ReleasePlugin._
import sbtrelease.Version

object ApplicationBuild extends Build {

  // credentials required when accessing atlassian-proxy-internal
  credentials := Seq(Credentials(Path.userHome / ".ivy2" / ".credentials"))

  lazy val customResolvers = Seq(
    Resolver.defaultLocal,
    "atlassian-proxy-internal" at "https://m2proxy.atlassian.com/content/groups/internal/",
    "atlassian-proxy-public" at "https://m2proxy.atlassian.com/content/groups/public/",
    "atlassian-maven-public" at "http://maven.atlassian.com/content/groups/public/",
    Classpaths.typesafeReleases,
    DefaultMavenRepository,
    Resolver.sbtPluginRepo("releases"),
    Resolver.mavenLocal
  )

  lazy val standardSettings = 
    scalariformSettings ++
    graphSettings ++
    releaseSettings ++
    Seq[Def.Setting[_]] (
      scalaVersion := "2.11.7",
      crossScalaVersions := Seq("2.10.5", "2.11.7"),
      scalacOptions ++= Seq("-Xfatal-warnings", "-feature", "-language:higherKinds", "-target:jvm-1.7"),
      resolvers ++= customResolvers,
      organization := "com.atlassian.connect.playscala.extensions",
      publishTo <<= version { (v: String) =>
        val repo = "https://maven.atlassian.com/"
        if (v.trim.endsWith("SNAPSHOT"))
          Some("Atlassian Repositories" at repo + "content/repositories/atlassian-public-snapshot")
        else
          Some("Atlassian"  at repo + "content/repositories/atlassian-public")
      },
      mappings in (Compile, packageBin) += (main.base / "LICENSE.txt") -> "META-INF/LICENSE.txt",
      ReleaseKeys.crossBuild := true,
      ReleaseKeys.versionBump := Version.Bump.Bugfix,
      publishMavenStyle := true,
      pomExtra := (
        <organization>
            <name>Atlassian</name>
            <url>http://www.atlassian.com/</url>
        </organization>
        <url>https://bitbucket.org/atlassianlabs/atlassian-connect-play-scala-extensions</url>
        <licenses>
          <license>
            <name>Apache License 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0</url>
            <distribution>repo</distribution>
          </license>
        </licenses>
        <scm>
          <url>https://bitbucket.org/atlassianlabs/atlassian-connect-play-scala-extensions/src</url>
          <connection>scm:git:git@bitbucket.org:atlassianlabs/atlassian-connect-play-scala-extensions.git</connection>
          <developerConnection>scm:git:git@bitbucket.org:atlassianlabs/atlassian-connect-play-scala-extensions.git</developerConnection>
        </scm>
      )
    )

  lazy val standardSettingsWithCommonDependencies =
    standardSettings ++
      AllDependencies.commonDependencies ++
      AllDependencies.commonTestDependencies


  lazy val acHostRedisStore = Project(
    id = "ac-host-redis-store", base = file("ac-host-stores/ac-host-redis-store"), settings = standardSettingsWithCommonDependencies
  ).settings(
    description := "Redis Database Configuration for AcHostStore"
  )

  lazy val main: Project = Project(
    id = "all", base = file("."), settings = standardSettings
  ) settings (
    description := "This is a collection of extensions for the Atlassian Connect Play Scala module."
  )aggregate (acHostRedisStore)
}
