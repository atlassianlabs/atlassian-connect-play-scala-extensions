import play.Play.autoImport._
import sbt._
import Keys._

object AllDependencies {

  val acPlayScalaVersion = "0.4.1"

  lazy val commonDependencies = libraryDependencies ++= Seq(
    "com.atlassian.connect" %% "ac-play-scala" % acPlayScalaVersion
  )

  lazy val commonTestDependencies = libraryDependencies ++= Seq(
    "org.specs2"     %% "specs2"     % "2.3.13"  % "test",
    "org.scalacheck" %% "scalacheck" % "1.11.3" % "test",
    "org.mockito" % "mockito-all" % "1.9.5" % "test",
    "junit"          %  "junit"      % "4.11"   % "test"
  )

  lazy val redisDependencies = Seq(
    resolvers ++= Seq(
      "org.sedis Maven Repository" at "http://pk11-scratch.googlecode.com/svn/trunk"
    ),
    libraryDependencies ++= Seq(
      "com.typesafe.play.plugins" %% "play-plugins-redis" % "2.3.1",
      cache
    ))
}
